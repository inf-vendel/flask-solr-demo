# Flask & Solr Project

This is a project that uses Flask and Solr to build a web application that allows users to search and retrieve data from a Solr collection.

## Prerequisites

Before you can run this project, you will need to have the following software installed on your system:

- Docker
- Docker Compose

## Setup

To set up this project, follow these steps:

1. Clone this repository:

    ```bash
    git clone https://github.com/[YOUR_USERNAME]/[YOUR_REPO_NAME].git
    ```

2. Navigate to the project directory:

    ```bash
    cd [YOUR_REPO_NAME]
    ```

3. Update your .env file:

    In the .env file, add the following environment variables:

    ```conf
    COLLECTION_NAME=programs
    UPLOAD_FOLDER_NAME=uploads
    ```

4. Run the project with Docker Compose:

    ```bash
        docker-compose up
    ```

5. Usage

To use this project, open a web browser and go to <http://localhost:5000>. You should see the Flask & Solr application.

## Details

The webapp:
    - The flask application is defined in the `/app` folder.
    - The requirements are defined in the `requirements.txt`.
    - The Dockerfile contains a minimal build for this app.

Docker compose:
    - Starts up a Solr (8.6.3) instance and creates a volume. For this you need to define the COLLECTION_NAME env variable.
    - Builds and launches the flask app in unbuffered binary stdout mode, so prints will be visible on the terminal (useful for debugging).
    - Uses port forwarding for both applications.
