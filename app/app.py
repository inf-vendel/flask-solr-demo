from flask import Flask, render_template, request, flash, redirect, url_for
import pysolr
import requests
import json
from dotenv import load_dotenv
import os
from werkzeug.utils import secure_filename
import json
from requests_kerberos import HTTPKerberosAuth, OPTIONAL
kerberos_auth = HTTPKerberosAuth(mutual_authentication=OPTIONAL, sanitize_mutual_error_response=False)

load_dotenv()

COLLECTION_NAME = os.environ["COLLECTION_NAME"]
ALLOWED_EXTENSIONS = {"json", "txt"}

# Load UPLOAD_FOLDER_NAME from env and create a folder
UPLOAD_FOLDER = os.path.join(os.path.abspath(os.path.dirname(__file__)), os.environ["UPLOAD_FOLDER_NAME"])
isDirExists = os.path.exists(UPLOAD_FOLDER)
if not isDirExists:
    os.mkdir(UPLOAD_FOLDER)

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = os.urandom(12).hex()
solr = pysolr.Solr(f"http://solr:8983/solr/{COLLECTION_NAME}", timeout=10, always_commit=True, auth=kerberos_auth)

FIELD_NAMES = {
        "id":"id",
        "title": "programmeProperties_programmeTitle_s",
        "series_title": "programmeProperties_seriesTitle_s",
        "genre": "programmeProperties_genre_s",
        "date": "programmeProperties_season_s",}

@app.route('/', methods=["GET","POST"])
def index():
    query = ""
    search_type = ""
    term = ""
    if request.method == "POST":
        if not collections_exist():
            flash('No data found.')
            return redirect(request.url)
        term = request.form["search-term"]
        if term == "" or term is None:
            term = "*"
        search_type = FIELD_NAMES[request.form.get("search-type")]
        query = f'{search_type}:*{term}*'

    response = solr.search(query, rows=20, **{}).__dict__
    num_of_results = response['raw_response']['response']['numFound']
    docslist = response['raw_response']['response']['docs']
    result = response['docs']
    return  render_template('index.html', num_of_results=num_of_results, results=result, headers=FIELD_NAMES)


def collections_exist():
    response = solr.search(collection='*', q='*:*', rows=0).__dict__
    num_of_results = response['raw_response']['response']['numFound']
    return bool(num_of_results)



@app.route('/upload', methods=["GET", "POST"])
def upload():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part.')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file.')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            result = add_to_collection(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash(result)
            return redirect(url_for('index'))

    return render_template('upload.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def add_to_collection(file):
    data = None
    with open(file) as f:
        data = json.load(f)
    try:
        solr.add(data)
    except Exception as e:
        return e
    return "Successfully added to documents."


if __name__ == '__main__':
    app.run(host='0.0.0.0')
